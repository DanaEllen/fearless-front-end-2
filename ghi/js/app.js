function createCard(name, description, pictureUrl, startDate, endDate, location) {
  return `
    <div class="card">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle text-muted">${location}</h6> <!-- Subtitle for location -->
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
        Starts: ${startDate}<br>
        Ends: ${endDate}
      </div>
    </div>
  `;
}


window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      throw new Error('Network response not ok');
    } else {
      const data = await response.json();

      const column = document.querySelector('.col');

      for (const conference of data.conferences) {
        const detailURL = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailURL);
    
        if (detailResponse.ok) {
            const details = await detailResponse.json();
    
            const startDate = new Date(details.start).toLocaleDateString('en-US');
            const endDate = new Date(details.end).toLocaleDateString('en-US');
    
            const html = createCard(details.name, details.description, details.conference.location.picture_url, startDate, endDate, details.conference.location.name);
    
    
          column.innerHTML += html; // Append card HTML to the column
        } else {
          throw new Error('Detail response not ok');
        }
      }
    }
  } catch (e) {
    console.error(e);
    // Handle error
  }
});
